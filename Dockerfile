FROM public.ecr.aws/lambda/python:3.7

ARG LAMBDA_TASK_ROOT="/var/task/"

COPY src/* ${LAMBDA_TASK_ROOT}/
COPY data/* ${LAMBDA_TASK_ROOT}/data/
COPY requirements.txt .
RUN pip install --upgrade pip && \
    pip install -v -r requirements.txt \
    --no-dependencies \
    --target ${LAMBDA_TASK_ROOT}
